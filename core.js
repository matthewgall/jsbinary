/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
$(document).ready(function() {

	$('#txtnumber').keyup(function() {

		// First, we take the value, and store it for later lulz
		var value = parseInt( $('#txtnumber').val() );

		// A simple zero check
		if ( !value ) {

			// Set result of -> decimal to 0
			$('#dec').val("");
			// Set result of -> binary to 0
			$('#bin').val("");
			// Set result of -> hexidecimal to 0
			$('#hex').val("");
			// Set result of -> hexidecimal to 0
			$('#oct').val("");

		}
		else {

			// The number will always be decimal so
			$('#dec').val( value );
	        	// Now convert decimal to binary and display the result
	        	$('#bin').val( parseInt( value.toString(2) ) );
			// Now to convert decimal to hexadecimal and display the result
			var hex = value.toString(16);
			$('#hex').val( hex.toUpperCase() );
			// Now to convert decimal to octal and display the result
			var oct = value.toString(8);
			$('#oct').val( oct.toUpperCase() );
			
		}
 
	});

	$('#demo').click(function() {

		// First, we take the value, and store it for later lulz
		var value = parseInt( $('#txtnumber').val() );

		// And clear the methods box
		$('#methods').append( "---------------------------------------\r\n" );
		
		// Now, we're going to have to loop through the number, calculating the modulus and the remainder and displaying this to the user
		// First, we're going to show them how to do binary
		$('#methods').append( "Binary:\r\n" );

		var q = value;
		var i = 0;
		
		// Now, we're using a while loop as we're not sure how long this is going to run for
		while (q > 0) {

			// Now, we need two variables, the whole number modulus and the remainder of the division by 2
			var v = q
			var r = parseInt(q%2);
			q = parseInt(q/2);

			// And now we print the calculation
			$('#methods').append( v + " divided by 2 = " + q + " r " + r + "\r\n");

			// And now we increment 1
			i++;
			
		
		}
		

		// Now, we're going to have to loop through the number, calculating the modulus and the remainder and displaying this to the user
		// First, we're going to show them how to do octal
		$('#methods').append( "Octal:\r\n" );

		var q = value;
		var i = 0;
		
		// Now, we're using a while loop as we're not sure how long this is going to run for
		while (q > 0) {

			// Now, we need two variables, the whole number modulus and the remainder of the division by 8
			var v = q
			var r = parseInt(q%8);
			q = parseInt(q/8);

			// And now we print the calculation
			$('#methods').append( v + " divided by 8 = " + q + " r " + r + "\r\n");

			// And now we increment 1
			i++;
			
		
		}

		// Now, we're going to have to loop through the number, calculating the modulus and the remainder and displaying this to the user
		// First, we're going to show them how to do hexadecimal
		$('#methods').append( "Hexadecimal:\r\n" );

		var q = value;
		var i = 0;
		
		// Now, we're using a while loop as we're not sure how long this is going to run for
		while (q > 0) {

			// Now, we need two variables, the whole number modulus and the remainder of the division by 16
			var v = q
			var r = parseInt(q%16);
			q = parseInt(q/16);

			// Now to do our number -> letter replacement
			switch (r) {

				case 10:
					r = "A";
					break;

				case 11:
					r = "B";
					break;

				case 12:
					r = "C";
					break;

				case 13:
					r = "D";
					break;

				case 14:
					r = "E";
					break;

				case 15:
					r = "F";
					break;
				
				default:
					r = r;
					break;
			}
			
			// And now we print the calculation
			$('#methods').append( v + " divided by 16 = " + q + " r " + r + "\r\n");

			// And now we increment 1
			i++;
			
		
		}
	});

	$('#reset').click(function() {

		// Now to reset, so we clear our data values
		location.reload();
	
	});

});
